﻿Shader "Tutorial/Tut1" {
	Properties {
		_MainColor("Main Color", Color) = (1, 1, 1, 1)
	}
	SubShader {
		Pass {
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				
				float4 _MainColor;
				
				struct v2f {
					float4 pos : SV_POSITION;
				};
				
				v2f vert(appdata_base v)
				{
					v2f o;
					o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					return o;
				}
				
				half4 frag(v2f i) : COLOR
				{
					return half4 (_MainColor);
				}
			ENDCG
		}
	} 
}
